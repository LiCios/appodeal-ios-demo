//
//  AppodealNetworkNames.h
//  Appodeal
//
//  Created by Ivan Doroshenko on 07/07/15.
//  Copyright (c) 2015 Appodeal, Inc. All rights reserved.
//

FOUNDATION_EXPORT NSString *const kAppodealAdColonyNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealAdMobNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealAdMobVideoNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealAmazonAdsNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealAppLovinNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealChartboostNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealFacebookNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealInMobiNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealMyTargetNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealMyTargetBannerNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealMopubNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealMRAIDNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealOpenXNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealTapSenceNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealUnityAdsNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealVungleNetworkName;
FOUNDATION_EXPORT NSString *const kAppodealSmaatoNetworkName;

FOUNDATION_EXPORT NSString * AppodealPrettyNetworkName(NSString *networkName);
